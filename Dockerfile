FROM node
COPY . /workspace
RUN mkdir /workspace/logs
WORKDIR /workspace
ENTRYPOINT ["node", "index.js"]